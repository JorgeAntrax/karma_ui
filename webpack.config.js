const path = require("path");

module.exports = {
	mode: "development",
	watch: true,
	watchOptions: {
		ignored: "**/node_modules",
		poll: 1000,
		aggregateTimeout: 600,
	},
	resolve: {
		alias: {
		  src: path.resolve(__dirname, 'src'),
		  components: path.resolve(__dirname, 'src/components'),
		  core: path.resolve(__dirname, 'src/core'),
		},
		extensions: ['.js', '.jsx'],
	  },
	entry: {
		app: ["./index.js"],
	},
	output: {
		path: path.join(__dirname, "dist"),
		filename: "[name].js",
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				use: {
					loader: "babel-loader",
					options: { presets: [["@babel/preset-env"]] },
				},
			},
			{
				test: /\.(ts|tsx)/,
				exclude: /node_modules/,
				use: ["ts-loader"],
			},
		],
	},
};
