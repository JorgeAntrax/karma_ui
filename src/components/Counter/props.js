/*
* @return Array props
* @description reactive observedAttributes
*/

export default [
	"min",
	"max",
	"step",
	"value"
]