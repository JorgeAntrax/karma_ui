import { jsx, connect, useState, useEffect } from 'core';
import { tag, styles, props } from './config';
import { Host } from 'core/decorators';

function Counter ( props )
{
	const [counter, setCounter] = useState( 1 );
	const [_step, setStep] = useState( 1 );
	const [_min, setMin] = useState( 1 );
	const [_max, setMax] = useState( 10 );

	props.value = props.value || 1;
	props.step = props.step || 1;
	props.min = props.min || 1;
	props.max = props.max || 10;

	useEffect( () =>
	{
		console.log( props )
		const { min, max, step, value } = props;

		+step !== NaN && setStep( +step );
		+min !== NaN && setMin( +min );
		+max !== NaN && setMax( +max );
		+value !== NaN && setCounter( +value );
	}, [] )

	const handleNext = () =>
	{
		const value = counter >= _min ? counter - _step : _min
		setCounter( value )
	}

	const handleBack = () =>
	{
		const value = counter < _max ? counter + _step : _max
		setCounter( value )
	}

	return jsx(
		<Host stylesheet={ { styles, props } }>
			<button onClick={ () => handleNext() }>
				menos
			</button>
			<span>{ counter }</span>
			<button onClick={ () => handleBack() }>
				mas
			</button>
		</Host>
	)
}

export default connect( Counter, { tag, props } )