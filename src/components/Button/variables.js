export default {
	base: {
		primary: "--ui-button-primary",
		secondary: "--ui-button-secondary",
		success: "--ui-button-success",
		warning: "--ui-button-warning",
		danger: "--ui-button-danger",
		dark: "--ui-button-dark",
		color: "--ui-button-color"
	}
}