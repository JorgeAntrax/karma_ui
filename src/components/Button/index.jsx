import { jsx, connect } from 'core';
import { tag, styles, props } from './config';
import { Host } from 'core/decorators';

function Button ( props ) {
	return jsx(
		<Host stylesheet={ { styles, props } }>
			<button>
				<slot></slot>
			</button>
		</Host>
	)
}

export default connect( Button, { tag, props } )