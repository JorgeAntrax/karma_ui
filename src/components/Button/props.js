// Observable properties
export default [
	"primary",
	"secondary",
	"success",
	"warning",
	"danger",
	"dark",
	"color",
	"flat",
	"bordered",
	"transparent",
	"floating",
	"square",
	"circle",
	"href",
	"to",
]