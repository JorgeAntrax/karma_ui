import css, {
	theme,
	getTheme,
	parseToRGB,
	mapToProp,
	customCSSProps,
} from "core/css";
import _button from "./variables";

const { base } = _button;
const { colors } = theme;

const {
	$padding,
	$margin,
	$radius,
	$line,
	$width,
	$height,
	$family,
	$size,
	$weight,
} = customCSSProps;

const host = ({ radius, square, circle }) => css`
	:host {
		all: initial;
		display: inline-block;
		position: relative;
	}

	:host([hidden]) {
		display: none;
	}

	:host([disabled]) {
		pointer-events: none;
		cursor: not-allowed;
		opacity: 0.6;
	}

	:host button {
		outline: 0;
		cursor: pointer;
		user-select: none;
		position: relative;
		display: inline-flex;
		align-items: center;
		align-content: center;
		justify-content: center;
		border: 2px solid transparent;
		padding: var(${$padding}, 10px);
		margin: var(${$margin}, 3px);
		height: var(${$height}, 40px);
		min-width: var(${$height}, 40px);
		width: var(${$width}, auto);
		font-family: var(${$family}, sans-serif);
		font-size: var(${$size}, 16px);
		font-weight: var(${$weight}, normal);
		line-height: var(${$line}, 1);
		border-radius: var(
			${$radius},
			${circle ? '100%' : square ? "5px" : radius ? mapToProp(radius) : "15px"}
		);
	}
	:host button:focus {
		outline: 0;
	}
`;

const getCustomColor = (type, style, color, text) => {
	let result = "";
	switch (type) {
		case "flat":
			const background = parseToRGB(color, 0.3);
			const textbase = parseToRGB(color);

			result = css`
				:host([flat]) button {
					color: var(${base[style]}-text, ${textbase});
					background-color: var(${base[style]}-bg, ${background});
					transition: background-color 200ms linear,
						color 250ms linear;
				}
				:host([flat]) button:hover {
					transform: none;
					box-shadow: none;
					color: var(${base[style]}-text-hovered, ${text});
					background-color: var(
						${base[style]}-bg-hovered,
						${textbase}
					);
				}
			`;
			break;

		case "bordered":
			const _color = style === 'warning' ? colors.dark : 'white'; 
			result = css`
				:host([bordered]) button {
					background-color: var(${base[style]}-bg, transparent);
					border: 2px solid var(${base[style]}-border, ${color});
					color: var(${base[style]}-text, ${color});
					transition: all 100ms linear;
				}
				:host([bordered]) button:hover {
					border-color: var(
						${base[style]}-border,
						${parseToRGB(color, 0.6)}
					);
				}

				:host([bordered]) button:active,
				:host([bordered]) button:focus {
					border-color: var(${base[style]}-active-border, ${color});
					background-color: var(${base[style]}-active-bg, ${color});
					color: var(${base[style]}-active-text, ${_color});
				}
			`;
			break;
		case 'transparent':
			result = css`
				button {
					background-color: transparent;
					color: var(${base[style]}-text, ${color});
					transition: transform 250ms linear, box-shadow 200ms linear;
					overflow: hidden;
				}
				button:before {
					content: '';
					display: block;
					width: 0;
					height: 0;
					position: absolute;
					top: 50%;
					left: 50%;
					transform: translate(-50%, -50%);
					background-color: ${parseToRGB(color, 0.2)};
					transition: width 600ms ease, height 600ms ease, background-color 400ms ease-out;
					border-radius: 10rem;
				}
				button:hover::before {
					width: 10rem;
					height: 10rem;
				}
				button:active::before {
					background-color: ${parseToRGB(color, 0.4)};
				}
			`;
			break;
		case 'floating':
			result = css`
				button {
					background-color: var(${base[style]}-bg, ${color});
					color: var(${base[style]}-text, ${text});
					transition: transform 250ms linear, box-shadow 200ms linear;
					box-shadow: 0 5px 25px -8px var(${base[style]}-shadow, ${parseToRGB(color)});
				}
				button:hover {
					transform: translateY(-3px);
				}

				button:active {
					transform: translateY(0);
					box-shadow: none;
				}
			`;
			break;
		default:
			result = css`
				button {
					background-color: var(${base[style]}-bg, ${color});
					color: var(${base[style]}-text, ${text});
					transition: transform 250ms linear, box-shadow 200ms linear;
				}
				button:hover {
					transform: translateY(-3px);
					box-shadow: 0 5px 25px -8px var(${base[style]}-shadow, ${parseToRGB(color)});
				}
			`;
			break;
	}
	return result;
};

const createBasicTheme = (data) => {
	const { style, text } = getTheme(data);
	return !style ? '' : getCustomColor(null, style, colors[style], text);
};

const createBorderTheme = (data) => {
	const { style, text } = getTheme(data);
	return !style ? '' : getCustomColor('bordered', style, colors[style], text);
};

const createFlatTheme = (data) => {
	const { style, text } = getTheme(data);
	return !style ? '' : getCustomColor('flat', style, colors[style], text);
};

const createTraslucentTheme = (data) => {
	const { style, text } = getTheme(data);
	return !style ? '' : getCustomColor('transparent', style, colors[style], text);
};

const createFloatingTheme = (data) => {
	const { style, text } = getTheme(data);
	return !style ? '' : getCustomColor('floating', style, colors[style], text);
};

const styles = ({
	circle,
	radius,
	square,
	flat,
	bordered,
	transparent,
	floating,
	primary,
	secondary,
	success,
	warning,
	danger,
	dark,
	color,
}) => {
	const styles = {
		primary,
		secondary,
		success,
		warning,
		danger,
		dark,
		color,
	};

	let result = host({ circle, radius, square });

	if (!!flat) {
		result += createFlatTheme(styles);
	} else if (!!bordered) {
		result += createBorderTheme(styles);
	} else if (!!transparent) {
		result += createTraslucentTheme(styles);
	} else if(!!floating) {
		result += createFloatingTheme(styles);
	} else {
		result += createBasicTheme(styles);
	}

	return result;
};
export default styles;
