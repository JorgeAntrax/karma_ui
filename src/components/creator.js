const fs = require( 'fs' );
const path = require('path');
const readline = require('readline');

const input = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const index = fs.readFileSync( './src/core/base/index.txt', 'utf-8' );
const config = fs.readFileSync( './src/core/base/config.txt', 'utf-8' );
const styles = fs.readFileSync( './src/core/base/styles.txt', 'utf-8' );
const props = fs.readFileSync( './src/core/base/props.txt', 'utf-8' );


function onCreateWebComponent ()
{
	input.question('Name of web component in CamelCase: ', (value) => {
		const name = value;
		fs.mkdir(path.join(__dirname, `${name}`), (err) => {
			if (err) {
				return console.error(err);
			}

			fs.writeFileSync( `./src/components/${ name }/index.jsx`, index.replace(/Component/g, name) );
			fs.writeFileSync( `./src/components/${ name }/config.js`, config );
			fs.writeFileSync( `./src/components/${ name }/styles.js`, styles );
			fs.writeFileSync( `./src/components/${ name }/props.js`, props );
			
			let exports = fs.readFileSync( './src/components/index.js', 'utf-8' );
			exports += `\nexport { default as ${name} } from './${name}';`;

			fs.writeFileSync( `./src/components/index.js`, exports );

			console.log('Directory created successfully!');
		});

		input.close();
	});
}

onCreateWebComponent();