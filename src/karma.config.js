const theme = {
	colors: {
		primary: "#0089FF",
		secondary: "#233D4D",
		success: "#26E28A",
		warning: "#FFD639",
		danger: "#EE2E31",
		dark: "#232525",
		light: "#f1f1f1",
		white: "#FFFFFF",
	},
	shadows: {
		primary: "#0089FF",
		secondary: "#233D4D",
		success: "#26E28A",
		warning: "#FFD639",
		danger: "#EE2E31",
		light: "#f1f1f1",
		white: "#FFFFFF",
		dark: "rgba(0, 0, 0,0.3)",
	},
};

export default theme;