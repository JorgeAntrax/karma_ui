const Fragment = "Fragment";
const customAttributes = ["children", "key", "props"];
const privateAttributes = ( key ) => !customAttributes.includes( key.toLowerCase() );

function h ( type, config )
{
	if ( typeof type === "function" )
	{
		if ( type.prototype !== undefined )
		{
			return new type( config );
		}
		return type( config );
	}

	const { children = [], ...props } = config;
	const childrenProps = Array().concat( children );

	return {
		type,
		key: null,
		props: {
			...props,
			children: childrenProps.map( ( child ) =>
			{
				return typeof child === "object"
					? child
					: {
						type: "textNode",
						key: null,
						props: {
							text: child,
							children: [],
						},
					};
			} ),
		},
	};
}

const render = function ( node )
{
	if ( typeof node === "string" )
	{
		return node.length
			? document.createTextNode( node )
			: document.createTextNode( "" );
	}

	if ( node.type === "textNode" )
	{
		if ( node.props.text === undefined )
		{
			node.props.text = "";
		}
		const textElem = document.createTextNode( node.props.text );
		return textElem;
	}

	const element =
		node.type === "Fragment"
			? document.createDocumentFragment()
			: document.createElement( node.type );

	const props = node.props;
	const propKeys = Object.keys( props );

	if ( props?.className )
	{
		if ( Array.isArray( props.className ) )
		{
			props.className = props.className.join( " " );
		} else if ( typeof props.className === "object" )
		{
			const _class = [];
			for ( let cls in props.className )
			{
				_class.concat( props[cls] );
			}
			props.className = _class.join( "" );
		}
		props.class = props.className;
	}
	// assign attributes
	console.log(propKeys)
	propKeys && propKeys.filter( privateAttributes ).forEach( ( name ) =>
	{
		if ( name )
		{
			let attrVal = props[name];
			element[name] = attrVal;
			element[name.toLowerCase()] = attrVal;
		}
	} );

	if ( node?.props?.children?.length )
	{
		node.props.children
			.map( render )
			.forEach( ( child ) => element.appendChild( child ) );
	}

	return element;
};

export { h as jsx, h as jsxs, h as jsxDEV, render, Fragment };