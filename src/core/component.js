import { component } from "haunted";

const getComponentName = ( string ) => {
	let result = string.replace( /([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2' ).toLowerCase();
	return result.charAt( 0 ) === "-" ? result.slice( 1 ) : result
}

export function connect (element, { tag, props }) {
	element.observedAttributes = props;
	const name = tag || getComponentName( element.name );
	name && window.customElements.define( name, component( element ) )
}