import { render } from 'core'
/**
 * @param {children} param0
 * @type {JSXObject}
 * @description {The web component content}
 * 
 * @param {stylesheet} param1
 * @type {object}
 * @description {Have two proprties: styles and props for styles rendering} 
 * @returns 
 */

const Host = ( { children, stylesheet } ) =>
{
	const { styles, props } = stylesheet;

	return (
		<>
			<style>{ styles( props ) }</style>
			{ children && ( <>{ children }</> ) }
		</>
	)
}

export { Host }