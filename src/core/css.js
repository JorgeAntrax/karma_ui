import { theme, getTheme, customCSSProps, isDarkenColor } from './theme';
const util = (function () {
	// Thanks to Andrea Giammarchi
	var reEscape = /[&<>'"]/g,
		reUnescape = /&(?:amp|#38|lt|#60|gt|#62|apos|#39|quot|#34);/g,
		oEscape = {
			"&": "&amp;",
			"<": "&lt;",
			">": "&gt;",
			"'": "&#39;",
			'"': "&quot;",
		},
		oUnescape = {
			"&amp;": "&",
			"&#38;": "&",
			"&lt;": "<",
			"&#60;": "<",
			"&gt;": ">",
			"&#62;": ">",
			"&apos;": "'",
			"&#39;": "'",
			"&quot;": '"',
			"&#34;": '"',
		},
		fnEscape = function (m) {
			return oEscape[m];
		},
		fnUnescape = function (m) {
			return oUnescape[m];
		},
		replace = String.prototype.replace,
		minify = (string) =>
			string
				.replace(/[\r\t]/g, " ")
				.replace(/[\n]/g, " ")
				.replace(/^\s+|\s+$|\s+(?=\s)/g, "");
	return (Object.freeze || Object)({
		escape: function escape(s) {
			return replace.call(s, reEscape, fnEscape);
		},
		unescape: function unescape(s) {
			return replace.call(s, reUnescape, fnUnescape);
		},
		minify: (string) => {
			return string
				.replace(/[\r\t]/g, " ")
				.replace(/[\n]/g, " ")
				.replace(/^\s+|\s+$|\s+(?=\s)/g, "");
		},
	});
})();

// Tagged template function
function css(pieces) {
	var result = pieces[0];
	var substitutions = [].slice.call(arguments, 1);
	for (var i = 0; i < substitutions.length; ++i) {
		result += util.escape(substitutions[i]) + pieces[i + 1];
	}

	result = util.minify(result);
	return result;
}

const parseToRGB = (hex, alpha = 1) => {
	const [r, g, b] = hex.match(/\w\w/g).map((x) => parseInt(x, 16));
	return `rgba(${r},${g},${b},${alpha})`;
};

const mapToProp = (prop) => {
	prop = prop.split(",");
	return prop.join("px ") + "px".trim();
};

export default css;
export { parseToRGB, mapToProp, theme, getTheme, customCSSProps, isDarkenColor };
